package com.example.customadapter;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);
        ListView countingList = (ListView) findViewById(R.id.lvCounting);
        countingList.setAdapter(new CountingListAdapter(getLayoutInflater()));
        countingList.setOnItemClickListener(parent, view, position, id);
        Toast.makeText(view.getContext(), (String) parent.getAdapter().getItem(position),
                Toast.LENGTH_LONG.show();
    });
}

  @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action car if it is present.
      getMenuInflater().inflate(R.menu.menu_main_list, menu);
      return  true;
  }

